Smoky Valley Contracting strives to achieve A+ workmanship on every job. With over 12 years class 1 driving and equipment operation, we assure personalized customer service, proper delivery, and planning assistance with any project. We are proud to offer both commercial and residential services.

Address : 2095 Bernau Ct, Lake Country, BC V4V 2M7, Canada

Phone No : 778-363-0056

Website : http://www.smokyvalley.ca
